Project Summary
Información General
Nombre: Radar HF (Multi estatico) 
Area: Operaciones
Preparado por: Marco Milla, Ramiro Yanque, Karim Kuyeng, Josemaria Gomez, Alexander Valdez.

Equipo
Nombre Rol
Alexander Valdez Responsable
Josemaría Gomez Responsable (Hasta Dic 2017, regreso en Agosto 2018 :)
Luis Vilcatoma Responsable
Ramiro Yanque Asesor
Karim Kuyeng Supervisora (Desde Marzo 2018)
Marco Milla Usuario

Objetivos General
El proyecto consiste en construir un radar multiestatico de monitoreo de la Ionosfera en dos frecuencias (2.72 MHz y 3.64 MHz) HF. Este sistema tendrá 2 estaciones de transmisión y 6 estaciones de recepción. Actualmente se cuenta con una estación transmisora ubicada en Ancón con transmisor de 5 W y dos estaciones receptoras IGP-JRO, IGP Sede Huancayo.
Versión 1.1 Mayo 2017
El proyecto consiste en construir un radar multiestatico de monitoreo de la Ionosfera en dos frecuencias (2.72 MHz y 3.64 MHz) HF. Este sistema tendrá 3 estaciones de transmisión y 7 estaciones de recepción. Actualmente se cuenta con una estación transmisora ubicada en Ancón con transmisor de 5 W y dos estaciones receptoras IGP-JRO, IGP Sede Huancayo.

Objetivos Específicos
Para la segunda etapa se contempla los siguientes objetivos:
- Tener 2 estaciones de transmisión
- Tener 4 estaciones de recepción
- Tener datos por el menos el 90% de cada mes por cada estación.
- Tener los datos de Jicamarca con un retraso menor a 4 días. 
- Tener los datos de las otras estaciones con un retraso menor a 14 días
- Almacenar los resultados en una pagina web
http://jro-app.igp.gob.pe/hfdatabase/webhf/web_signalchain/rawdata_test_new/#
Versión 1.1 Mayo 2017

- Tener 3 estaciones de transmisión
- Tener 7 estaciones de recepción
- Tener datos por el menos el 90% de cada mes por cada estación.
- Tener los datos de Jicamarca con un retraso menor a 3 días. 
- Tener los datos de las otras estaciones con un retraso menor a 10 días
- Almacenar los resultados en una pagina web
http://jro-app.igp.gob.pe/hfdatabase/webhf/web_signalchain/rawdata_test_new/#

Presupuesto: IGP Donaciones Radar HF
Fecha de Inicio: 01/04/2017
Fecha de Fin: 16/10/2017

Versión 1.1 Mayo 2017
Fecha de Inicio: 01/04/2017
Fecha de Fin: 09/08/2021
Fecha de Presentación Final: 04/10/2021
